import math
import Shapes


def test_circle_area():
    test_circle = Shapes.circle(1.0)
    assert test_circle.area() == math.pi
    test_circle = Shapes.circle(2.0)
    assert test_circle.area() == (math.pi * 4)

def test_circle_perimeter():
    test_circle = Shapes.circle(1.0)
    assert test_circle.perimeter() == (math.pi * 2)
    test_circle = Shapes.circle(2.0)
    assert test_circle.perimeter() == (math.pi * 4)

def test_rectangle_area():
    test_rect = Shapes.rectangle(1,1)
    assert test_rect.area() == 1
    test_rect = Shapes.rectangle(2,2)
    assert test_rect.area() == 4

def test_rectangle_perimeter():
    test_rect = Shapes.rectangle(1,1)
    assert test_rect.perimeter() == 4
    test_rect = Shapes.rectangle(2,2)
    assert test_rect.perimeter() == 8


print ("Starting UT - testing circle area")
test_circle_area()
print ("Starting UT - testing circle perimeter")
test_circle_perimeter()
print ("Starting UT - testing rectangle area")
test_rectangle_area()
print ("Starting UT - testing rectangle perimeter")
test_rectangle_perimeter()



